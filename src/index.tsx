import React, { Component, useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

// MODELS

interface Squares {
    squares: any[]
}

interface GameState {
    history: Squares[],
    stepNumber: number,
    xIsNext: boolean,
}

// SQUARE

function Square(props: any): JSX.Element {
    return (
        <button
            className="square"
            onClick={props.onClick}
        >
            {props.value}
        </button>
    );
}

function renderSquare(props: any, i: number): JSX.Element {
    return (
        <Square
            value={props.squares[i]}
            onClick={() => {
                props.onClick(i)
            }}
        />
    );
}

// BOARD

function Board(props: any): JSX.Element {
    return (
        <div>
            <div className="board-row">
                {renderSquare(props, 0)}
                {renderSquare(props, 1)}
                {renderSquare(props, 2)}
            </div>
            <div className="board-row">
                {renderSquare(props, 3)}
                {renderSquare(props, 4)}
                {renderSquare(props, 5)}
            </div>
            <div className="board-row">
                {renderSquare(props, 6)}
                {renderSquare(props, 7)}
                {renderSquare(props, 8)}
            </div>
        </div>
    );
}

// GAME

function Game(): JSX.Element {
    const [gameState, setGameState] = useState({
        history: [{
            squares: Array(9).fill(null),
        }],
        stepNumber: 0,
        xIsNext: true,
    } as GameState)
    const current = gameState.history[gameState.history.length - 1]
    const winner = calculateWinner(current.squares);
    let status = winner ? 'Winner: ' + winner : 'Next player: ' + (gameState.xIsNext ? 'X' : 'O');

    const moves = gameState.history.map((step, stepNumber) => {
        const desc = stepNumber ?
            'Go to move #' + stepNumber :
            'Go to game start';
        return (
            <li key={stepNumber}>
                <button onClick={() => {
                    const historyToSet = gameState.history.slice(0, stepNumber + 1)
                    setGameState({
                        ...gameState,
                        history: historyToSet,
                        stepNumber: stepNumber,
                        xIsNext: (stepNumber % 2) === 0,
                    })
                }}>{desc}</button>
            </li>
        );
    });

    return (
        <div className="game">
            <div className="game-board">
                <Board
                    squares={current.squares}
                    onClick={(i: number) => setGameState(handleClick(i, gameState))}
                />
            </div>
            <div className="game-info">
                <div>{status}</div>
                <ol>{moves}</ol>
            </div>
        </div>
    );
}

function handleClick(i: number, gameState: GameState): GameState {
    const history = gameState.history.slice(0, gameState.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
        return gameState
    }
    squares[i] = gameState.xIsNext ? 'X' : 'O'
    return {
        ...gameState,
        history: history.concat([{
            squares: squares
        }]),
        stepNumber: history.length,
        xIsNext: !gameState.xIsNext,
    }
}

// OTROS METODOS

function calculateWinner(squares: Squares[]): Squares | any {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
}

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);